//
//  FlashCardSetCollectionCell.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashcardSetCollectionCell: UICollectionViewCell
{
    @IBOutlet var textLabel: UILabel!
    
    static let identifier = "SetCell"
    
    public func configure(with text: String) {
        textLabel.text = text
        textLabel.textColor = .white
        textLabel.font = textLabel.font.withSize(40)
        textLabel.sizeToFit()
        self.backgroundColor = UIColor(red: 68/255, green: 53/255, blue: 69/255, alpha: 1)
    }
    
}
