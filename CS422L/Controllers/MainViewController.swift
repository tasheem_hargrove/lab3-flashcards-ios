import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet var mainContainer: UIStackView!
    @IBOutlet var collectionView: UICollectionView!
    var sets: [FlashcardSet] = [FlashcardSet]()
    @IBOutlet var headingStackView: UIStackView!
    @IBOutlet var heading: UILabel!
    @IBOutlet var addBtn: UIButton!
    
    @IBAction func addSet(_ sender: Any) {
        let set = FlashcardSet()
        let nextCount = sets.count + 1
        set.title = "Title \(nextCount)"
        sets.append(set)
        collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let setClass = FlashcardSet()
        //connect hard coded collection to sets
        sets = FlashcardSet.getHardCodedCollection()
        
        mainContainer.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        heading.textColor = .white
        heading.sizeToFit()
        
        headingStackView.backgroundColor = UIColor(red: 68/255, green: 53/255, blue: 69/255, alpha: 1)
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return number of items
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: FlashcardSetCollectionCell.identifier, for: indexPath) as! FlashcardSetCollectionCell
        
        //setup cell display here
        cell.configure(with: sets[indexPath.row].title)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)
        collectionView.deselectItem(at: indexPath, animated: true)
        print("You tapped me")
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let windowRect = self.view.window?.frame;
        if windowRect != nil {
            let windowWidth = windowRect!.size.width / 2 - 10;
            return CGSize(width: windowWidth, height: CGFloat(150))
        }
        
        return CGSize(width: CGFloat(300), height: CGFloat(100))
    }
}
